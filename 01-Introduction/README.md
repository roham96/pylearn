# Introduction

## addition(+)
  - print(2 + 3)   

## subtraction(-)
  - print(3 - 1)   
## multiplication(*)
  - print(2 * 3)   
## division(/)
  - print(3 / 2)   
## exponential(**)
  - print(3 ** 2)
## modulus(%)
  - print(3 % 2)  
## Floor division operator(//)
  - print(3 // 2) 

<hr width="100%">

# Checking data types

## Int
  - print(type(10))

## Float
  - print(type(3.14)) 

## Complex
  - print(type(1 + 3j)) 

## String
  - print(type('Asabeneh'))

## List
  - print(type([1, 2, 3]))

## Dictionary
  - print(type({'name':'Asabeneh'})) 

## Set
  - print(type({9.8, 3.14, 2.7})) 
  
## Tuple
  - print(type((9.8, 3.14, 2.7)))                 
               
             
          
           

   
  